const moment = require('moment');
const getHolidays = require( "./holidaysAPI");
const { Holiday } = require("../models/holiday")





const currentMonthHolidays = async () => {
    const currentDate = moment(Date.now());
    const currentYear = 2023;

    const supportedCountires =["FR","UK"];
    const listAnne = ["2013","2014","2015","2016","2017","2018","2019","2020","2021","2022","2023","2024"]
    for ( const anne of listAnne){
    for (const country of supportedCountires) {
        try {
            const apiHolidays = await getHolidays(country, anne);
           // console.log("apiHolidays",apiHolidays);
            if (!Array.isArray(apiHolidays)) continue;
            let holidays = {};
            apiHolidays.forEach((h) => {
                if (h.type[0].toLowerCase() !== "national holiday") return;
                const month = h && h.date && h.date.datetime && h.date.datetime.month;
                if (!month) return;
                holidays =
                    (Array.isArray(holidays) && holidays) || [];
                const date = h && h.date && h.date.datetime && h.date.datetime;
                if (!date) return;
                holidays.push({
                    date,
                    name: h && h.name,
                    contry: country

                });
                console.log("hhhhhhhhhhh",h)
            });
            console.log("holidays",holidays);
            if (!Object.keys(holidays).length) continue;
            for (const mh of holidays) {
                try {
                    // holi = new Holiday();
                    // holi.name=mh.name;
                    // holi.date.year = mh.date.year;
                    // holi.date.month = mh.date.month;
                    // holi.date.day = mh.date.day;
                    await Holiday.findOneAndUpdate(
                         { name: mh.name},
                         { date: { year: mh.date.year,month: mh.date.month,day: mh.date.day} },
                         { upsert: true }
                    );
                } catch (e) {
                    console.error("could not save holidays", e);
                }
            }
        } catch (e) {
            console.error("could not get holidays from API", e);
        }
    }}
};
module.exports =currentMonthHolidays;