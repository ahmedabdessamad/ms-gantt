const  axios =require("axios");

const API_BASE = "https://calendarific.com/api/v2/holidays";
const getHolidays = async (country, year) => {
    const apiResponse = await axios.get(API_BASE, {
        params: {
            country,
            year,
            type: "national",
            api_key: '3fafdb328fffca1db648894359e42e970b59326f'
        }
    });
    return apiResponse.data.response.holidays;
};
module.exports= getHolidays;