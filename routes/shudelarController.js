const express = require("express");
const router = express.Router();
const ObjectID = require("mongoose").Types.ObjectId;
const asyncHandler = require("express-async-handler");
const { Appointment } = require("../models/appointment");
const { Placement } = require("../models/placement");
const { Holiday } = require("../models/holiday");
const { ClientCorporation } = require("../models/clientCorporation");
const currentMonthHolidays = require ("./holiday")
//import Appointment from "../models/appointment";
const axios =require ("axios");

const url = 'mongodb://localhost:27017/calendar';


// router.post('/holidays',function (req,res,next){
//     let item ={
//         name: "ahmed",
//
//         date: {
//             year: 2011,
//             month: 1,
//             day: 1,
//         }
//     };
//     mongo.connect(url,function (err,db)
//     {
//     assert.equal(null,err);
//     db.collection('holiday').insertOne(item,function (err,result){
//         assert.equal(null,err);
//         console.log('itemmmm inserteddddddddddddddddddddddddddd');
//         db.close();
//     })
//     })
// })


router.get(
  "/appointment",
  asyncHandler(async (req, res) => {
    //res.send(req.params.id)
     client = req.params.idc;
    try {
      const appot = [];
      const shedular55 = await Appointment.find();
      shedular55.map((s) => {
        let newS = {
          Id: s.id,
          startDate: new Date(s.dateBegin),
          endDate: new Date(s.dateEnd),
          Name:
            s.candidateReference && s.candidateReference.firstName
              ? `${s.candidateReference.firstName} ${s.candidateReference.lastName}`
              : "Candidate",
          progress: 50,

          IsReadonly: true,
        };
        appot.push(newS);
      });

      if (!appot) {
        return res.status(400).json({ message: "not found " }).end();
      }

      res.send(appot);
    //  console.log(appot);
    } catch (e) {
      console.error(e);
      res.status(400).end();
    }
  })
);

router.get(
  "/appointment/:idc",
  asyncHandler(async (req, res) => {
    //res.send(req.params.id)
     client = req.params.idc;
    try {
      const appointment = [];
      const result = [];
      const palcment = [];
      const shedular55 = await Appointment.find({
        "clientContactReference.id": client
      });
      shedular55.map((s) => {
        // console.log(s.dateEnd);
        const diffTime = Math.abs( new Date(s.dateEnd) -  new Date(s.dateBegin));
        const dure = Math.ceil(Math.abs( new Date(s.dateEnd) -  new Date(s.dateBegin))/(1000 * 60 * 60 * 24));
        const consome = Math.ceil(Math.abs( new Date(s.dateEnd) -  new Date(Date.now()))/(1000 * 60 * 60 * 24));
        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
        console.log("difffff",dure,consome);
        let newS = {
          TaskId: s.id,
          StartDate: new Date('01/01/2018'),
         // EndDate:  new Date(s.dateEnd),
          Duration: diffDays,
          TaskName:
            s.owner && s.owner.firstName
              ? `${s.owner.firstName} ${s.owner.lastName}`
              : "Candidate",
              ParentId: 1,
              Progress: 50

        };

        appointment.push(newS);
      });
      const shedular66 = await Placement.find({"clientContact.id": client});
      shedular66.map((p)=>{
          let newSx = {
              Id:p.id,
              StartTime: new Date(p.dateBegin),
              EndTime: new Date(p.dateEnd),
              Subject:  p.owner && p.owner.firstName ? `${p.owner.firstName} ${p.owner.lastName}`: "Candidate",
              Description: p.status,
             // Location:"Inconnue ",
              IsReadonly:true,
              ResourceID:2,

          };
          appointment.push(newSx);
      });

      if (!appointment) {
        return res.status(400).json({ message: "not found " }).end();
      }

      result.push(appointment);
      //result.push(palcment);
      res.send(appointment);
       console.log(appointment);
    } catch (e) {
      console.error(e);
      res.status(400).end();
    }
  })
);



router.get(
    "/placement/:idc",
    asyncHandler(async (req, res) => {



       client = req.params.idc;
        try {
            const placement = [];

            const shedular55 = await Placement.find({
                "clientCorporation.id": client
            });
            shedular55.map((s) => {
              //  console.log(s.dateEnd);
                //const diffTime1 = Math.abs( new Date(s.dateEnd) -  new Date(s.dateBegin));
                const duretotal = (Math.abs(( new Date(s.dateEnd))/(1000 * 60 * 60 * 24) -  (new Date(s.dateBegin))/(1000 * 60 * 60 * 24)));
                let days = 0 ;
               let dateT = Math.round((s.dateEnd-s.dateBegin)/(1000*60*60*24));

                const dureconsome =(Math.abs((new Date(Date.now()))/(1000 * 60 * 60 * 24)) - (new Date(s.dateBegin)/(1000 * 60 * 60 * 24)) );
                //const diffDays1 = (diffTime1 / (1000 * 60 * 60 * 24));
                let prog = Math.round((dureconsome/duretotal)*100);
                const pp = new Date (s.dateBegin).getDay()
                console.log("ahmed jours",pp);
                    if ( pp < 6)
                    {

                        const tt = 7 - pp -2;
                        const aa = 7- pp ;
                        const work =  ((dateT-aa) % 7);
                        const workingDays= Math.trunc(((dateT-aa)/7)*5);
                        days = tt + work + workingDays;
                    }else {


                    }
                if (prog>100){
                    prog=100;
                }
                    console.log("geeeeeeeeeeeeeeeeeeeeeeeeeet day :",days)


                let newS = {
                    TaskId:s.candidate.id,
                    TaskName:
                        s.candidate && s.candidate.firstName
                            ? `${s.candidate.firstName} ${s.candidate.lastName}`
                            : "Candidate",
                    StartDate:  new Date(s.dateBegin),
                    // EndDate:  new Date(s.dateEnd),
              Duration: days,
                     Progress: prog,
                    ParentId: 1



                };

                placement.push(newS);
        });

            if (!placement) {
                return res.status(400).json({ message: "not found " }).end();
            }
            //const b = JSON.stringify(placement);
             res.contentType('application/json');


            console.log("les placement:",placement);
            res.send(placement);

        } catch (e) {
            console.error(e);
            res.status(400).end();
        }
    })
);

router.get(
    "/placementdata/:idc",
    asyncHandler(async (req, res) => {
        //res.send(req.params.id)
   client = req.params.idc;
        try {
            const placement = [];

            const shedular55 = await Placement.find({
                "clientContact.id": client
            });
           await shedular55.map((s) => {
                console.log(s.dateEnd);
                const diffTime1 = Math.abs( new Date(s.dateEnd) -  new Date(s.dateBegin));
                const dure1 = Math.ceil(Math.abs( new Date(s.dateEnd) -  new Date(s.dateBegin))/(1000 * 60 * 60 * 24));
                const consome1 = Math.ceil(Math.abs( new Date(s.dateEnd) -  new Date(Date.now()))/(1000 * 60 * 60 * 24));
                const diffDays1 = Math.ceil(diffTime1 / (1000 * 60 * 60 * 24));
                const prog = Math.round((consome1/dure1)*100);

                let newS = {
                    TaskId: s.id,
                    TaskName:
                        s.owner && s.owner.firstName
                            ? `${s.owner.firstName} ${s.owner.lastName}`
                            : "null",
                    StartDate: new Date(s.dateBegin),
                    clientContactReference:s.clientContactReference && s.clientContactReference.firstName
                        ? `${s.clientContactReference.firstName} ${s.clientContactReference.lastName}`
                        : "null",
                    // EndDate:  new Date(s.dateEnd),
                    Duration: diffDays1,
                };

                placement.push(newS);
            });

            if (!placement) {
                return res.status(400).json({ message: "not found " }).end();
            }

            res.contentType('application/json');
            await res.send({Items:placement});
            console.log(placement);
        } catch (e) {
            console.error(e);
            res.status(400).end();
        }
    })
);

router.get(
    "/apt/:idc",
    asyncHandler(async (req, res) => {
        //res.send(req.params.id)
      client = req.params.idc;
        try {
            const apt = [];

            const shedular55 = await Appointment.find({
                "clientCorporation.id": client
            });
            shedular55.map((s) => {

                          let newS = {

                    Id: s.id,
                    StartTime:  new Date(s.dateBegin),
                    EndTime:  new Date(s.dateEnd),
                    Subject: s.owner && s.owner.firstName
                            ? `${s.owner.firstName} ${s.owner.lastName}`
                            : "Candidate",
                     IsReadonly:true,
                     ResourceID:1,
                     Location: s.location,
                     Description:s.subject

                };

                apt.push(newS);
            });
                console.log("les appointment:",apt)
            if (!apt) {
                return res.status(400).json({ message: "Not Found" }).end();
            }

            res.contentType('application/json');



            res.send(apt);

        } catch (e) {
            console.error(e);
            res.status(400).end();
        }
    })
);

router.get(
    "/Holiday",
    asyncHandler(async (req, res) => {
        try {
            const shedular55 = await Holiday.find({
               date:{
                   $gte:{year:2019,month:1,day:1},
                   $lt:{year:2019,month:12,day:31}
               }
            }).count();
            const b = JSON.stringify(shedular55);
             res.contentType('application/json');

            console.log("le nombre de jour est:",shedular55);
            res.send(b);

        } catch (e) {
            console.error(e);
            res.status(400).end();
        }
    })
);

router.get(
    "/aptgantt/:ref",
    asyncHandler(async (req, res) => {
        //res.send(req.params.id)
        //const gg = await  currentMonthHolidays;
        //gg()


        client = req.params.ref;
        try {
            const clientRef = [];

            const shedular55 = await ClientCorporation.find({
                "id": client
            });

            shedular55.map((s) => {

                let newS = {

                     contactRef:s.clientContacts.data,

                };

                clientRef.push(newS);
            });



            console.log("les placement dans le clientRef:",clientRef);
            res.send(clientRef);

        } catch (e) {
            console.error(e);
            res.status(400).end();
        }

    })

);


router.get(



    "/aptpro2/:ref",
    asyncHandler(async (req, res) => {

        const client = req.params.ref;
        let array = [];
        try {
            await axios.get('http://localhost:5000/api/v2/aptgantt/'+client)
                .then(function (response) {
                    let j = 0;


                    for (let i = 0; i < response.data[0].contactRef.length; i++) {
                        array[j] = response.data[0].contactRef[i].id;

                        j++;

                        JSON.stringify(array);

                    }
                    console.log("array",array)
                })

        } catch (err) {
            console.error(err);
        }


        try {
            const dateevent = [];

            const shedular55 = await Appointment.find({
                "clientContactReference.id": {$in : array},'gantt':1
            });
            shedular55.map((s) => {

                let newS = {
                    Id: s.id,
                    StartTime:  new Date(s.dateBegin),
                    EndTime:  new Date(s.dateEnd),
                    Subject: s.clientContactReference && s.clientContactReference.firstName
                        ? `${s.clientContactReference.firstName} ${s.clientContactReference.lastName}`
                        : "null",
                    clientContactReference:s.clientContactReference && s.clientContactReference.firstName
                        ? `${s.clientContactReference.firstName} ${s.clientContactReference.lastName}`
                        : "null",
                    IsReadonly:true,
                    ResourceID:1,
                    Location: s.location,
                    Description:s.subject


                };
                dateevent.push(newS);
            });
            res.send(dateevent);
            console.log("dateevent:",dateevent)

        } catch (e) {
            console.error(e);
            res.status(400).end();
        }

    })
);

router.get(



    "/aptpro/:ref",
    asyncHandler(async (req, res) => {

        const client = req.params.ref;
        let array = [];
        try {
            await axios.get('http://localhost:5000/api/v2/aptgantt/'+client)
                .then(function (response) {
                    let j = 0;


                    for (let i = 0; i < response.data[0].contactRef.length; i++) {
                        array[j] = response.data[0].contactRef[i].id;

                        j++;

                        JSON.stringify(array);

                    }
                    console.log("array",array)
                })

        } catch (err) {
            console.error(err);
        }


        try {
            const dateevent = [];

            const shedular55 = await Appointment.find({
                "clientContactReference.id": {$in : array}
            });
            shedular55.map((s) => {

                let newS = {
                    Id: s.id,
                    StartTime:  new Date(s.dateBegin),
                    EndTime:  new Date(s.dateEnd),
                    Subject: s.clientContactReference && s.clientContactReference.firstName
                        ? `${s.clientContactReference.firstName} ${s.clientContactReference.lastName}`
                        : "null",
                    clientContactReference:s.clientContactReference && s.clientContactReference.firstName
                        ? `${s.clientContactReference.firstName} ${s.clientContactReference.lastName}`
                        : "null",
                    IsReadonly:true,
                    ResourceID:1,
                    Location: s.location,
                    Description:s.subject


                };
                dateevent.push(newS);
            });
            res.send(dateevent);
            console.log("dateevent:",dateevent)

        } catch (e) {
            console.error(e);
            res.status(400).end();
        }

    })
);



module.exports = router;


