const mongoose = require("mongoose");

const Placement = mongoose.model(
    "placement",
    {
        id: Number,
        dateAdded: Number,
        dateBegin: Number,
        dateEnd: Number,
        clientContact: {
            id: Number,
            firstName: String,
            lastName: String,
            location: String,
        },
        clientCorporation: {
            id: Number,
            name:String
        },
        candidate: {
            id: Number,
            firstName: String,
            lastName: String,
        },
        owner: {
            id: Number,
            firstName: String,
            lastName: String,
          },
        status:String,
    },


    "placement"
);

module.exports = { Placement };