const mongoose = require("mongoose");

const ClientCorporation = mongoose.model(
    "clientCorporation",
    {
        id: Number,
        clientContacts:{
            data:[]
        },
        annualRevenue:Number
    },


    "clientCorporation"
);

module.exports = { ClientCorporation };