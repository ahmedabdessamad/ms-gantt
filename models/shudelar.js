const mongoose = require("mongoose");

const Shudelar = mongoose.model(
  "shudelar",
  {
    id: Number,
    location: String,
    Subject:String,
      StartTime:String,
      EndTime:String,
      Duration: Number,
      ParentId: Number,
      Progress: Number,

  },
  "shudelar"
);

module.exports = { Shudelar };
