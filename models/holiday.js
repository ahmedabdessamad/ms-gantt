const mongoose = require("mongoose");

const Holiday = mongoose.model(
    "holiday",
    {
        id: Number,
        name: String,

        date: {
            year: Number,
            month: Number,
            day: Number,
        }

    },


    "holiday"
);

module.exports = { Holiday };